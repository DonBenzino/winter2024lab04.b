public class Panda 
{
	private String name;
	private String type;
	private int age;
	
	public Panda(String name, String type, int age){
		this.name = name;
		this.type = type;
		this.age = age;
	}
	
	public void askPanda() {
		if (this.type.equals("Giant Panda")) {
			System.out.println(name + "Wants some pets?");
		}else if (this.name.equals("Po")) {
			System.out.println("Stop being lazy, do you want to get hit by Master Shifu?");
		}else {
			System.out.println(name + "Want a snack?");
		}
	}
	public void doActivity() {
		if (this.age < 8) {
			System.out.println(name + "Go play with your friends.");
		}else if (this.name.equals("Po")){
			System.out.println("Po go make some noodles");
		}else {
			System.out.println(name + "Go relax yourself, take a nap.");
		}
	}
	
	public String getName(){
		return this.name;
	}
	public String getType(){
		return this.type;
	}
	public int getAge(){
		return this.age;
	}
	
	public void setAge(int age){
		this.age = age;
	}
}

