import java.util.Scanner;

public class VirtualPetApp
{
	public static void main (String [] args) {
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		Panda[] embarrassment = new Panda[1];
		
		for(int i = 0; i < embarrassment.length; i++){
			String tempName = "";
			String tempType = "";
			int tempAge = 0;
			
			System.out.println("Enter panda type");
			tempType = reader.nextLine();
			System.out.println("Enter panda name");
			tempName = reader.nextLine();
			System.out.println("Enter panda age");
			tempAge = Integer.parseInt(reader.nextLine());
			
			embarrassment[i] = new Panda(tempName, tempType, tempAge);
		}

		System.out.println(embarrassment[0].getName() + " " + embarrassment[0].getType() + " " + embarrassment[0].getAge());
		System.out.println("Change the age");
		embarrassment[0].setAge(Integer.parseInt(reader.nextLine()));
		System.out.println(embarrassment[0].getName() + " " + embarrassment[0].getType() + " " + embarrassment[0].getAge());

	}
}